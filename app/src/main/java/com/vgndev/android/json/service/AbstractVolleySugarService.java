package com.vgndev.android.json.service;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.vgndev.android.json.util.GsonRequest;
import com.vgndev.android.json.util.RequestObjectPoolSingleton;
import com.vgndev.android.json.util.RequestQueueSingleton;

/**
 * Created by Dan on 05/03/2016.
 */
public abstract class AbstractVolleySugarService<T>
{
    private final Context context;
    private final RequestQueue queue;
    private RequestObjectPoolSingleton pool;

    public AbstractVolleySugarService(final Context context)
    {
        this.context = context;
        this.queue = RequestQueueSingleton.getInstance(context).getRequestQueue();
        this.pool = RequestObjectPoolSingleton.getInstance(context);
    }

    public void get(Context context, String url, T type)
    {
        Class clazz = type.getClass();

        GsonRequest<T> myReq = new GsonRequest<T>(
                Request.Method.GET,
                url,
                clazz,
                createMyReqSuccessListener(),
                createMyReqErrorListener());


        queue.add(myReq);
    }

    private Response.Listener<T> createMyReqSuccessListener() {
        return new Response.Listener<T>() {
            @Override
            public void onResponse(T response) {
                // TODO save response in a map within the pool - no need to save it locally!!
//                pool.put(response.getClass().);

            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Do whatever you want to do with error.getMessage();
            }
        };
    }
}
