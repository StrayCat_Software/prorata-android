package com.vgndev.android.json.util;

import android.content.Context;
import android.util.Log;

import java.util.Map;

/**
 * Singleton pool for runtime storage and access to objects requested over a HTTP connection.
 *
 * @author Daniel Pawsey
 * @since 06/03/2016.
 */
public class RequestObjectPoolSingleton
{
    public final String TAG = this.getClass().getSimpleName();
    private static RequestObjectPoolSingleton mInstance;
    private Map<String, Object> objectMap;

    private RequestObjectPoolSingleton(Context context) {}

    public static synchronized RequestObjectPoolSingleton getInstance(Context context)
    {
        if (mInstance == null)
        {
            mInstance = new RequestObjectPoolSingleton(context);
        }
        return mInstance;
    }

    /**
     * Adds an object to the pool.
     *
     * @param key The key of the object to be added to the pool.
     * @param value The object to be added to the pool.
     */
    public void put(String key, Object value)
    {
        if(key != null && value != null)
        {
            Log.d(TAG, value.toString() + " added to pool with key " + key);
            objectMap.put(key, value);
        }
        else if(key != null && value == null)
        {
            String message = "Value must not be null";
            Log.e(TAG, message);
            throw new IllegalArgumentException(message);
        }
        else if(key == null && value != null)
        {
            String message = "Key must not be null";
            Log.e(TAG, message);
            throw new IllegalArgumentException(message);
        }
        else
        {
            String message = "Key and value must not be null";
            Log.e(TAG, message);
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * Gets an object from the pool.
     *
     * @param key The key of the object to be added to the pool.
     * @return The object requested from the pool.
     */
    public Object get(String key)
    {
        if(key != null)
        {
            if (objectMap.get(key) != null) {
                Log.d(TAG, key + " retrieved from pool.");
                return objectMap.get(key);
            } else {
                String message = "No object with that key can be found in the pool.";
                Log.e(TAG, message);
                throw new NullPointerException(message);
            }
        }
        else
        {
            String message = "Key must not be null";
            Log.e(TAG, message);
            throw new IllegalArgumentException(message);
        }
    }
}
