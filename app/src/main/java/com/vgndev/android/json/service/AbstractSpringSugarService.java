package com.vgndev.rest.service;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import static com.orm.SugarRecord.find;
import static com.orm.SugarRecord.save;

public abstract class AbstractSpringSugarService<T> {

    private final String TAG = this.getClass().getSimpleName();
    private RestTemplate restTemplate;
    private String baseUrl;
    private Context context;

    // TODO doc this constructor

    /**
     * @param baseUrl
     * @param context
     */
    public AbstractSpringSugarService(String baseUrl, Context context) {
        restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        this.baseUrl = baseUrl;
        this.context = context;
    }

    // TODO doc this method

    /**
     * @param clazz
     * @param requestBody
     * @param urlSuffix
     * @param rawQuery
     * @param criteria
     * @return
	 * @return
     */
    public T postForSame(Class<T> clazz, T requestBody, String urlSuffix, String rawQuery, String[] criteria) {

        String message = null;

        if (requestBody.getClass() == clazz) {

            T response = null;

            try {
                response = (T) restTemplate.postForObject(baseUrl + urlSuffix, requestBody, clazz);
                save(response);
                Log.d(TAG, "POST: Record retrieved from API and saved locally: " + response.toString());
            } catch (Exception e) {
                Log.e(TAG, e.getLocalizedMessage());
                response = (T) find(clazz, rawQuery, criteria).get(0);

                Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }

            if (response != null) {
                return response;
            } else {
                message = "POST: No data was retrieved from either the API or the local database.";
                Log.e(TAG, message);
                throw new NullPointerException(message);
            }
        } else {
            message = "POST: requestBody must be an instance of class T";
            Log.e(TAG, message);
            throw new IllegalArgumentException(message);
        }
    }

    public T getForSame(Class<T> clazz, String urlSuffix, String rawQuery, String[] criteria) {
        String message = null;
        T response = null;
        try {
            response = (T) restTemplate.getForObject(baseUrl + urlSuffix, clazz);
            save(response);
            Log.d(TAG, "POST: Record retrieved from API and saved locally: " + response.toString());
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage());
            response = (T) find(clazz, rawQuery, criteria).get(0);
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
        if (response != null) {
            return response;
        } else {
            message = "POST: No data was retrieved from either the API or the local database.";
            Log.e(TAG, message);
            throw new NullPointerException(message);
        }
    }
            Log.e(TAG, message);
            throw new NullPointerException(message);
        }
    }
}
