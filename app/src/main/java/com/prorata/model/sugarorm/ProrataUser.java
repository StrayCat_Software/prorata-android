/*
 * Created on 5 Mar 2016 ( Time 19:29:07 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */

package com.prorata.model.sugarorm;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.dsl.Table;
import com.prorata.model.jpa.AccountEntity;
import com.prorata.model.jpa.EmploymentEntity;
import com.prorata.model.jpa.ProrataUserEntity;
import com.prorata.model.jpa.SubscriptionEntity;
import com.prorata.model.jpa.UserContactEntity;

import java.util.ArrayList;

/**
 * SugarORM persistent class for entity stored in table "prorata_user"
 *
 * @author Daniel Pawsey
 */
@Table
public class ProrataUser extends ProrataUserEntity implements Parcelable {

    /**
	 * <p>
	 * This is for SugarORM SQLLite persistence only; it does not map to the
	 * remote database primary key column prorata_user_id of table 
     * prorata_user, so does not replace the remote Id attribute 
     * of the superclass.
	 * </p>
	 * 
	 * <p>
	 * This field is not used by the API, and should be ignored by it.
	 * </p>
	 */
    private Long id;

    public ProrataUser() {
        super();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.prorataUserId);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.email);
        dest.writeString(this.password);
        dest.writeString(this.address);
        dest.writeString(this.postcode);
        dest.writeList(this.listOfSubscription);
        dest.writeList(this.listOfUserContact);
        dest.writeList(this.listOfEmployment);
        dest.writeList(this.listOfAccount);
    }

    private ProrataUser(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.prorataUserId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.email = in.readString();
        this.password = in.readString();
        this.address = in.readString();
        this.postcode = in.readString();
        this.listOfSubscription = new ArrayList<SubscriptionEntity>();
        in.readList(this.listOfSubscription, SubscriptionEntity.class.getClassLoader());
        this.listOfUserContact = new ArrayList<UserContactEntity>();
        in.readList(this.listOfUserContact, UserContactEntity.class.getClassLoader());
        this.listOfEmployment = new ArrayList<EmploymentEntity>();
        in.readList(this.listOfEmployment, EmploymentEntity.class.getClassLoader());
        this.listOfAccount = new ArrayList<AccountEntity>();
        in.readList(this.listOfAccount, AccountEntity.class.getClassLoader());
    }

    public static final Creator<ProrataUser> CREATOR = new Creator<ProrataUser>() {
        public ProrataUser createFromParcel(Parcel source) {
            return new ProrataUser(source);
        }

        public ProrataUser[] newArray(int size) {
            return new ProrataUser[size];
        }
    };
}