/*
 * Created on 5 Mar 2016 ( Time 19:29:07 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */

package com.prorata.model.sugarorm;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.dsl.Table;
import com.prorata.model.jpa.ContractEntity;
import com.prorata.model.jpa.EmployerEntity;
import com.prorata.model.jpa.EmploymentEntity;
import com.prorata.model.jpa.EmploymentSessionEntity;
import com.prorata.model.jpa.PaymentEntity;
import com.prorata.model.jpa.ProrataUserEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * SugarORM persistent class for entity stored in table "employment"
 *
 * @author Daniel Pawsey
 */
@Table
public class Employment extends EmploymentEntity implements Parcelable {

    /**
	 * <p>
	 * This is for SugarORM SQLLite persistence only; it does not map to the
	 * remote database primary key column employment_id of table 
     * employment, so does not replace the remote Id attribute 
     * of the superclass.
	 * </p>
	 * 
	 * <p>
	 * This field is not used by the API, and should be ignored by it.
	 * </p>
	 */
    private Long id;

    public Employment() {
        super();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.employmentId);
        dest.writeString(this.name);
        dest.writeSerializable(this.hourlyRate);
        dest.writeLong(startDate != null ? startDate.getTime() : -1);
        dest.writeLong(endDate != null ? endDate.getTime() : -1);
        dest.writeSerializable(this.hoursPerWeek);
        dest.writeList(this.listOfEmploymentSession);
        dest.writeSerializable(this.prorataUser);
        dest.writeList(this.listOfContract);
        dest.writeList(this.listOfPayment);
        dest.writeSerializable(this.employer);
    }

    private Employment(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.employmentId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.hourlyRate = (BigDecimal) in.readSerializable();
        long tmpStartDate = in.readLong();
        this.startDate = tmpStartDate == -1 ? null : new Date(tmpStartDate);
        long tmpEndDate = in.readLong();
        this.endDate = tmpEndDate == -1 ? null : new Date(tmpEndDate);
        this.hoursPerWeek = (BigDecimal) in.readSerializable();
        this.listOfEmploymentSession = new ArrayList<EmploymentSessionEntity>();
        in.readList(this.listOfEmploymentSession, EmploymentSessionEntity.class.getClassLoader());
        this.prorataUser = (ProrataUserEntity) in.readSerializable();
        this.listOfContract = new ArrayList<ContractEntity>();
        in.readList(this.listOfContract, ContractEntity.class.getClassLoader());
        this.listOfPayment = new ArrayList<PaymentEntity>();
        in.readList(this.listOfPayment, PaymentEntity.class.getClassLoader());
        this.employer = (EmployerEntity) in.readSerializable();
    }

    public static final Parcelable.Creator<Employment> CREATOR = new Parcelable.Creator<Employment>() {
        public Employment createFromParcel(Parcel source) {
            return new Employment(source);
        }

        public Employment[] newArray(int size) {
            return new Employment[size];
        }
    };
}